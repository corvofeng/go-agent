# Go agent

## 项目构建

```bash
# Just build agent
go get -v github.com/op/go-logging 
go get -v github.com/coreos/etcd/client
go get -v github.com/gorilla/mux
go build -o go-agent .

# Or build docker
docker build . -t fyh_commit 
```

## 测试环境配置

来自[ bootstrap_samples.conf][1]

CpuPeriod = 50000

|type | 内存占用|  CPU Quota |
| --- | --- |    --- |
|consumer | 3G | 180000|
| small |  2G | 30000 |
| medium|  4G | 60000 |
| large |  6G | 90000 |

使用 (CPU Quota) / (CPU Period) 计算可得到当前container可使用的CPU占用率.

[Docker时钟周期的控制][2]


[1]: https://code.aliyun.com/middlewarerace2018/benchmarker/blob/master/workflow/bootstrap_samples.conf
[2]: https://blog.csdn.net/asd05txffh/article/details/52267818