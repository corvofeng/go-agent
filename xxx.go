package main

import "fmt"

func xxx() {
	var a, b, c int
	a = 10
	b = 20
	c = a + b

	g = a + b

	fmt.Printf("Hello world, %d, %d, %d, %d", a, b, c, sum(a, b))

	var gretting = "he ll"
	fmt.Println(len(gretting))

	var ip *int

	fmt.Printf("The value of ip is %x\n", ip)

	if ip == nil {
		fmt.Printf("ooooh fck\n")
	}

	ip = &a

	fmt.Printf("Address of a variable: %x\n", &a)

	fmt.Printf("Address stored in ip variable %x\n", ip)

	fmt.Printf("Value of *ip varivable: %d\n", *ip)

	sp := structVariableType{10, "say Hello"}

	fmt.Printf("struct s is %s\n", sp.x)

	/*
		var numbers [] int;

		PrintSlice(numbers)

		if(numbers == nil) {
			fmt.Printf("slice is nil")
		}
	*/

	numbers := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	PrintSlice(numbers)

	fmt.Println("numbers ==", numbers)
	fmt.Println("numbers[1:4] = ", numbers[1:4])
	fmt.Println("numbers[:3] = ", numbers[:3])
	fmt.Println("numbers[4:] = ", numbers[4:])

	numbers = append(numbers, 2, 3, 4)
	PrintSlice(numbers)

	for i := range numbers {
		fmt.Println("Slice item", i, " is ", numbers[i])
	}

	countryCapitaMap := map[string]string{
		"France": "Pairs",
		"Italy":  "Rome",
		"Japan":  "Tokyo",
	}

	for country := range countryCapitaMap {
		fmt.Println("Capital of ", country, " is ", countryCapitaMap[country])
	}

	for country, capital := range countryCapitaMap {
		fmt.Println("Capital of ", country, "is", capital)
	}
	sliceTest()
}

func sum(a, b int) int {
	fmt.Printf("value of a in sum() = %d\n", a)
	fmt.Printf("value of b in sum() = %d\n", b)

	return a + b
}

var g int

type structVariableType struct {
	a int
	x string
}

func sliceTest() {
	var ar = [10]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'}

	var a, b []byte

	a = ar[2:5]
	b = ar[3:5]

	fmt.Printf("ar: %v\n", ar)
	a[0] = 'x'
	fmt.Printf("a: %v, cap is %d\n", a, cap(a))
	fmt.Printf("b: %v\n", b)

}

func PrintSlice(x []int) {
	fmt.Printf("len = %d, cap = %d, slice=%v\n", len(x), cap(x), x)
}
