import urllib

import urllib.request
import json
import base64
import sys

etcd_root_path = 'go-agent'
etcd_service_name = 'com.alibaba.dubbo.performance.demo.provider.IHelloService'
etcd_url = 'http://etcd:2379'

weight_key = {
        'provider-small' : 1,
        'provider-medium' : 2,
        'provider-large' : 3,
}

def main():
    res = urllib.request.urlopen('{}/v2/keys/{}/{}'. \
                            format(etcd_url, etcd_root_path, etcd_service_name))
    info = res.read().decode("utf-8")
    jInfo = json.loads(info)
    nodes = jInfo['node']['nodes']
    prefix_len = len(etcd_root_path) + len(etcd_service_name) + 3


    servers = []

    for node in nodes:
        if node['value']:
            servers.append({
                'url': node['key'][prefix_len:],
                'weight': weight_key[node['value']]
                })



    with open("/root/upstream.conf", "w") as f:
        for s in servers:
            f.write('server {} weight={};\n'.format(s['url'], s['weight']))



if __name__ == "__main__":
    if sys.argv[1]:
       etcd_url = sys.argv[1]
    main()
