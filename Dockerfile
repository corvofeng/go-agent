
FROM golang:latest as go_builder
RUN mkdir /app
ADD . /app/
WORKDIR /app 
RUN go get -v github.com/pkg/profile
RUN go get -v github.com/op/go-logging 
RUN go get -v github.com/coreos/etcd/client
RUN go get -v github.com/gorilla/mux
RUN go build -o go-agent .

# Builder container
FROM registry.cn-hangzhou.aliyuncs.com/aliware2018/services AS builder

# COPY . /root/workspace/agent
# WORKDIR /root/workspace/agent
# RUN set -ex && mvn clean package

# Runner container
FROM registry.cn-hangzhou.aliyuncs.com/aliware2018/debian-jdk8

COPY --from=builder /root/workspace/services/mesh-provider/target/mesh-provider-1.0-SNAPSHOT.jar /root/dists/mesh-provider.jar
COPY --from=builder /root/workspace/services/mesh-consumer/target/mesh-consumer-1.0-SNAPSHOT.jar /root/dists/mesh-consumer.jar
#COPY --from=builder /root/workspace/agent/mesh-agent/target/mesh-agent-1.0-SNAPSHOT.jar /root/dists/mesh-agent.jar

COPY --from=builder /usr/local/bin/docker-entrypoint.sh /usr/local/bin

COPY --from=go_builder /app/go-agent /root/go-agent
COPY start-agent.sh /usr/local/bin
#COPY upstream.conf /root
COPY find_server.py /root

RUN set -ex \
 && chmod a+x /usr/local/bin/start-agent.sh \
 && chmod a+x /root/go-agent \
 && mkdir -p /root/logs

RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update && apt-get install -y nginx vim python3

COPY ngx.default /etc/nginx/nginx.conf 
EXPOSE 8087

ENTRYPOINT ["docker-entrypoint.sh"]
