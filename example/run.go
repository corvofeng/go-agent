package main

import (
	"fmt"
	"io"
	"net/http"
	"reflect"

	zk "./dubbo"
)

//zh.springboot.service.Address
type Address struct {
	City, Country string
}

//zh.springboot.service.Person
type Person struct {
	Age     int32
	Name    string
	Address Address
}

func main() {
	run_server()
}

func make_req() {
	var tcpAddress = "127.0.0.1:20889"
	javaClassMap := map[string]reflect.Type{
		"zh.springboot.service.Address": reflect.TypeOf(Address{}),
		"zh.springboot.service.Person":  reflect.TypeOf(Person{}),
	}

	// String interfaceName = "com.alibaba.dubbo.performance.demo.provider.IHelloService";
	// String method = "hash";
	// String parameterTypesString = "Ljava/lang/String;";
	// String parameter = "abcd";

	dubboCtx := &zk.DubboCtx{
		DubboVer:     "2.0.1",
		Service:      "com.alibaba.dubbo.performance.demo.provider.IHelloService",
		Method:       "hash",
		Version:      "null",
		Timeout:      5000,
		JavaClassMap: javaClassMap,
		Return:       reflect.TypeOf([]byte{}),
		// Return:       reflect.TypeOf([]Person{}),
	}

	//args map; key: arg_type value:arg_value
	args := make(map[string]interface{})
	// var srcArray []Person = make([]Person, 2)
	// address := Address{"上海", "中国"}
	// address2 := Address{"shanghai", "china"}
	// person := Person{12, "姓名", address}
	// person2 := Person{11, "xm", address2}
	// srcArray[0] = person
	// srcArray[1] = person2
	//	args["java.util.List"] = srcArray
	// String parameterTypesString = "Ljava/lang/String;";
	// String parameter = "abcd";
	args["parameterTypesString"] = "Ljava/lang/String;"
	args["parameter"] = "abcd"
	args["path"] = "com.alibaba.dubbo.performance.demo.provider.IHelloService"

	dubboCtx.Args = args
	fmt.Printf("send %s\n", args)

	ret, _ := zk.SendHession(tcpAddress, dubboCtx)
	fmt.Printf("ret %s\n", ret)

}

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello world!")
	make_req()
}

func run_server() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8000", nil)
}
