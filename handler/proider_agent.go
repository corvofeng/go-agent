package handler

import (
	"fmt"
	"io"
	"net/http"
	"time"
)
import zk "../dubbogo"
import util "../util"

func makeAsyncReq(iName string, method string,
	parameterType string, parameter string) string {

	dubboCtx := &zk.DubboCtx{
		DubboVer: "2.0.1",
		Service:  iName,
		Method:   method,
		Version:  "null",
		Timeout:  5000,
		Return:   nil, // reflect.TypeOf([]byte{}),
		// JavaClassMap: javaClassMap,
		// Return:       reflect.TypeOf([]Person{}),
	}

	args := make(map[string]interface{})
	args["parameterTypesString"] = parameterType
	args["parameter"] = parameter
	args["path"] = iName

	dubboCtx.Args = args
	fResponse, err := zk.MakeFutureResponse()
	util.CheckError(err, "make future")

	zk.MakeAsyncReq(fResponse, dubboCtx)
	time.Sleep(30 * time.Millisecond)	// 直接睡眠30ms

	defer zk.RequestOver(fResponse)
	log.Debugf("send %s to %d", args, fResponse.CliId)

	select {
	case realString := <-(fResponse.Response):
		log.Debugf("send %s Result: %s\n", args, realString)
		return realString
	case <-time.After(5 * time.Second): // 增加超时时间.
		log.Errorf("wait %d: %d too long", fResponse.CliId, fResponse.Id)
		return ""
	}
}

func makeReq(iName string, method string,
	parameterType string, parameter string) string {

	dubboPort := *util.GetConfig().DubboPortPtr
	var tcpAddress = fmt.Sprintf("127.0.0.1:%d", dubboPort)
	var log = util.GetLogger()
	log.Debug("Query: ", tcpAddress)

	dubboCtx := &zk.DubboCtx{
		DubboVer: "2.0.1",
		Service:  iName,
		Method:   method,
		Version:  "null",
		Timeout:  5000,
		Return:   nil, // reflect.TypeOf([]byte{}),
		// JavaClassMap: javaClassMap,
		// Return:       reflect.TypeOf([]Person{}),
	}

	args := make(map[string]interface{})
	args["parameterTypesString"] = parameterType
	args["parameter"] = parameter
	args["path"] = iName

	dubboCtx.Args = args
	log.Debugf("send %s\n", args)

	ret, _ := zk.SendHession(tcpAddress, dubboCtx)
	log.Debugf("ret %s\n", ret)
	return fmt.Sprintf("%s", ret)
}

//ProviderAsyncAgent 异步请求
func ProviderAsyncAgent(w http.ResponseWriter, r *http.Request) {
	// var log = util.GetLogger()
	r.ParseForm()
	interfaceName := r.FormValue("interface")
	method := r.FormValue("method")
	parameterTypesString := r.FormValue("parameterTypesString")
	parameter := r.FormValue("parameter")

	// log.Info(interfaceName)
	// log.Info(method)
	// log.Info(parameterTypesString)
	// log.Info(parameter)

	ret := makeAsyncReq(interfaceName, method, parameterTypesString, parameter)
	if ret == "" {
		w.WriteHeader(404)
	}
	io.WriteString(w, ret)

}

//ProviderAgent 同步请求
func ProviderAgent(w http.ResponseWriter, r *http.Request) {
	var log = util.GetLogger()
	r.ParseForm()
	interfaceName := r.FormValue("interface")
	method := r.FormValue("method")
	parameterTypesString := r.FormValue("parameterTypesString")
	parameter := r.FormValue("parameter")

	log.Info(interfaceName)
	log.Info(method)
	log.Info(parameterTypesString)
	log.Info(parameter)

	ret := makeReq(interfaceName, method, parameterTypesString, parameter)
	io.WriteString(w, ret)
}

// example request
// func makeReq(iName string, method string, parameterType string, parameter string) {
// 	var tcpAddress = "127.0.0.1:20889"
// 	dubboCtx := &zk.DubboCtx{
// 		DubboVer: "2.0.1",
// 		Service:  "com.alibaba.dubbo.performance.demo.provider.IHelloService",
// 		Method:   "hash",
// 		Version:  "null",
// 		Timeout:  5000,
// 		Return:   nil, // reflect.TypeOf([]byte{}),
// 		// JavaClassMap: javaClassMap,
// 		// Return:       reflect.TypeOf([]Person{}),
// 	}
//
// 	args := make(map[string]interface{})
// 	args["parameterTypesString"] = "Ljava/lang/String;"
// 	args["parameter"] = "abcd"
// 	args["path"] = "com.alibaba.dubbo.performance.demo.provider.IHelloService"
//
// 	dubboCtx.Args = args
// 	log.Debugf("send %s\n", args)
//
// 	ret, _ := zk.SendHession(tcpAddress, dubboCtx)
// 	log.Debugf("ret %s\n", ret)
// }
//
