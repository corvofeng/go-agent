package handler

import (
	"io"
	"net/http"
)

type Servers struct {
	url    string
	widget string
}

func ConsumerAgent(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	name := r.FormValue("name")
	log.Infof("name is: %s\n", name)

	io.WriteString(w, "Hello world!")
}
