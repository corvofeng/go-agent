#!/usr/bin/python3
import urllib

import urllib.request
import json
import base64
import random
import string


ORIG_URL = 'http://127.0.0.1:20000'
TEST_URL = 'http://127.0.0.1:30002'


def fetch_orig(headers, data):
    req = urllib.request.Request(ORIG_URL, data, headers)
    res = urllib.request.urlopen(req)
    body = res.read()
    return body

def fetch_test(headers, data):
    req = urllib.request.Request(TEST_URL, data, headers)
    res = urllib.request.urlopen(req)
    body = res.read()
    return body

COMPARE_STR = """----
parameter: {}
Orig body: {}
Test body: {}
----
"""


def test(parameter: str):

    data = {
            'interface': 'com.alibaba.dubbo.performance.demo.provider.IHelloService',
            'parameterTypesString': 'Ljava/lang/String;',
            'method': 'hash',
            'parameter': parameter
            }
    headers = {'contentType': "application/x-www-form-urlencoded"}
    post_data = urllib.parse.urlencode(data).encode('utf-8')

    test_body = fetch_test(headers, post_data)
    orig_body = fetch_orig(headers, post_data)

    if test_body != orig_body:
        print(COMPARE_STR.format(parameter, orig_body, test_body))
    else:
        print("Test OK!!!")

def rand_str(N):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))


def main_test():
    for i in range(8, 400):
        for cnt in range(5):
           test(rand_str(i))

if __name__ == "__main__":
#    test('abcd')
    main_test()
