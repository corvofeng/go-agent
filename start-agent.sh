#!/bin/bash

ETCD_HOST=etcd
ETCD_PORT=2379
ETCD_URL=http://$ETCD_HOST:$ETCD_PORT

echo ETCD_URL = $ETCD_URL

#  ./go-agent -type=provider-small -dubbo_port=20889 -cpu_profile=./logs/cpu.profile -mem_profile=./logs/mem.profile
if [[ "$1" == "consumer" ]]; then
  echo "Starting consumer agent..."
  # create /root/upstream.conf
  /usr/bin/python3 /root/find_server.py $ETCD_URL >> /root/logs/find_server.log
  /usr/sbin/nginx -g "daemon off;"
elif [[ "$1" == "provider-small" ]]; then
  echo "Starting small provider agent..."
  /root/go-agent -type=provider-small \
    -port=30000 \
    -dubbo_port=20880  \
    -cpu_profile=/root/logs/s-cpu.profile \
    -mem_profile=/root/logs/s-mem.profile \
    -etcd_url=$ETCD_URL 2&>1 > /root/logs/provider-small.log
elif [[ "$1" == "provider-medium" ]]; then
  echo "Starting medium provider agent..."
  /root/go-agent -type=provider-medium \
    -port=30000 \
    -dubbo_port=20880  \
    -cpu_profile=/root/logs/m-cpu.profile \
    -mem_profile=/root/logs/m-mem.profile \
    -etcd_url=$ETCD_URL 2&>1 > /root/logs/provider-medium.log
elif [[ "$1" == "provider-large" ]]; then
  echo "Starting large provider agent..."
  /root/go-agent -type=provider-large\
    -port=30000 \
    -dubbo_port=20880  \
    -cpu_profile=/root/logs/l-cpu.profile \
    -mem_profile=/root/logs/l-mem.profile \
    -etcd_url=$ETCD_URL 2&>1 > /root/logs/provider-large.log
else
  echo "Unrecognized arguments, exit."
  exit 1
fi
