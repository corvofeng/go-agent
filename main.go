/**
 *=======================================================================
 *    Filename:main.go
 *
 *     Version: 1.0
 *  Created on: May 23, 2018
 *
 *      Author: corvo
 *=======================================================================
 */

package main

/**
 * 2018-06-03: 当我把超时时间设置为3s时, 程序出现了 fatal error: runtime: out of memory
 *				料想是goroutine太多了, 导致出现这样的错误, 现在我决定设置连接量
 *				来限制goroutine数量.
 *	https://pauladamsmith.com/blog/2016/04/max-clients-go-net-http.html 可以限制并发的goroutine数量
 *
 *
 *	启动参数, 主要包括下列几项
 *  -Dtype=provider \
 *  -Dserver.port=30002\
 *  -Ddubbo.protocol.port=20891 \
 *  -Detcd.url=$ETCD_URL \
 *  -Dlogs.dir=/root/logs \
 *
 * 目前的版本使用短连接
 */

import (
	"syscall"
	//	"flag"
	"fmt"
	"github.com/gorilla/mux"
	// "net"
	"os"
	"os/signal"
	// "github.com/op/go-logging"
	//	"io"
	"net/http"
	"strings"
)

// import zk "./dubbogo"
import util "./util"
import handler "./handler"

var (
	log = util.GetLogger()
)

// MyServer 继续封装mux, 可以自动填充一些http请求头
type MyServer struct {
	r *mux.Router
}

func main() {
	// CPU profiling by default
	util.InitConfig()
	config := util.GetConfig()
	var err error

	//打开文件，若果文件不存在就创建一个同名文件并打开
	// outfile, err := os.OpenFile(*logF, os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	util.CheckError(err, "open file")
	util.InitLogger() // TODO: 添加文件写入

	log.Infof("type: %s", *config.TypePtr)
	log.Infof("port: %d", *config.PortPtr)
	log.Infof("dubbo port: %d", *config.DubboPortPtr)
	log.Infof("etcd url: %s", *config.EtcdUrlPtr)
	log.Infof("logs.dir: %s", *config.LogF)

	util.Register()
	util.ProfileStart()
	// zk.CreateAsyncDubboConnect(fmt.Sprintf("127.0.0.1:%d", *config.DubboPortPtr))
	// zk.LoopRun()

	runServer()
}

func runServer() {
	var config = util.GetConfig()
	log.Info("Start run server")
	stop := make(chan os.Signal, 1)

	router := mux.NewRouter().StrictSlash(true)
	if strings.HasPrefix(*config.TypePtr, "consumer") {
		log.Notice("Run in consumer mode")
		router.Methods("POST").Path("/").HandlerFunc(handler.ConsumerAgent)
	} else if strings.HasPrefix(*config.TypePtr, "provider-async") {
		log.Notice("Run in provider async mode")
		router.Methods("POST").Path("/").HandlerFunc(handler.ProviderAsyncAgent)
	} else if strings.HasPrefix(*config.TypePtr, "provider") {
		log.Notice("Run in provider async mode")
		router.Methods("POST").Path("/").HandlerFunc(handler.ProviderAsyncAgent)
		// log.Notice("Run in provider mode")
		// router.Methods("POST").Path("/").HandlerFunc(handler.ProviderAgent)
	} else {
		log.Error("Get wrong %s", config.TypePtr)
	}

	http.Handle("/", &MyServer{router})
	srv := &http.Server{
		Addr: fmt.Sprintf(":%d", *config.PortPtr),
	}
	// err := http.ListenAndServe(fmt.Sprintf(":%d", *config.PortPtr), nil)
	go runServerMain(srv)

	// from http://www.bite-code.com/2015/07/22/implementing-graceful-shutdown-for-docker-containers-in-go/
	//register for interupt (Ctrl+C) and SIGTERM (docker)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	<-stop
	log.Notice("Grace close")
	srv.Shutdown(nil)
	util.ProfileStop()
}

func runServerMain(srv * http.Server) {
	log := util.GetLogger()
	log.Notice("Listen start !!")
	err := srv.ListenAndServe()
	util.CheckError(err, "Listen failed")
	log.Notice("Listen end!!")

}

// 允许js进行跨域请求
func (s *MyServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		rw.Header().Set("Access-Control-Allow-Origin", origin)
		rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		rw.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}

	if req.Method == "OPTIONS" {
		return
	}
	s.r.ServeHTTP(rw, req)
}
