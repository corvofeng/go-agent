package util

import (
	"flag"
)
//Config 程序配置项
type Config struct {
	TypePtr         *string
	PortPtr         *int
	DubboPortPtr    *int
	EtcdUrlPtr      *string
	LogF            *string
	EtcdRootPath    *string
	EtcdServiceName *string
	CPUProfilePtr   *string
	MEMProfilePtr   *string
	Loglvl          *string
}

var config Config

func InitConfig() {
	config.TypePtr = flag.String("type", "consumer", "Type for program")
	config.PortPtr = flag.Int("port", 30002, "Agent listen")
	config.DubboPortPtr = flag.Int("dubbo_port", 20891, "Dubbo port")
	config.EtcdUrlPtr = flag.String("etcd_url", "http://127.0.0.1:2379", "etcd url")
	config.LogF = flag.String("log", "test.log", "Log file")
	config.CPUProfilePtr = flag.String("cpu_profile", "/root/logs/cpu.prof", "CPU Profile")
	config.MEMProfilePtr = flag.String("mem_profile", "/root/logs/mem.prof", "Memory profile" )
	config.Loglvl = flag.String("log_lvl", "INFO", "Log level" )


	etcdRootPath := "go-agent"
	etcdServiceName := "com.alibaba.dubbo.performance.demo.provider.IHelloService"

	config.EtcdRootPath = &etcdRootPath
	config.EtcdServiceName = &etcdServiceName

	flag.Parse()
}

func GetConfig() *Config {
	return &config
}
