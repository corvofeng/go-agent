package util

import (
	"context"
	"fmt"
	"github.com/coreos/etcd/client"
	"net"
	"os"
	"runtime"
	"time"
	"runtime/pprof"
)

//CheckError error check
func CheckError(err error, info string) (res bool) {
	var log = GetLogger()
	if err != nil {
		log.Error(info + "  " + err.Error())
		return false
	}
	return true
}

func checkErrorOld(err error, info string) (res bool) {
	if err != nil {
		fmt.Print(info + "  " + err.Error())
		return false
	}
	return true
}

//Register 按照如下方式进行注册
// 服务注册的key为:    /dubbomesh/com.some.package.IHelloService/192.168.100.100:2000
// String strKey = MessageFormat.format("/{0}/{1}/{2}:{3}",
//                  rootPath,serviceName,IpHelper.getHostIp(),String.valueOf(port));
func Register() {
	var log = GetLogger()

	cfg := client.Config{
		Endpoints: []string{*config.EtcdUrlPtr},
		Transport: client.DefaultTransport,
		// set timeout per request to fail fast when the target endpoint is unavailable
		HeaderTimeoutPerRequest: time.Second,
	}
	c, err := client.New(cfg)
	if err != nil {
		log.Fatal(err)
	}
	key := fmt.Sprintf("/%s/%s/%s:%d",
		*config.EtcdRootPath, *config.EtcdServiceName,
		GetIPAddr(), *config.PortPtr)

	kapi := client.NewKeysAPI(c)
	// set "/foo" key with "bar" value
	log.Info("Setting ", key)
	resp, err := kapi.Set(context.Background(), key, *config.TypePtr, nil)

	if err != nil {
		log.Fatal(err)
	} else {
		// print common key info
		log.Infof("Set is done. Metadata is %q\n", resp)
	}

}

//GetAllChanel 获取所有的频道, 目前不用
func GetAllChanel() {
	var log = GetLogger()

	cfg := client.Config{
		Endpoints: []string{*config.EtcdUrlPtr},
		Transport: client.DefaultTransport,
		// set timeout per request to fail fast when the target endpoint is unavailable
		HeaderTimeoutPerRequest: time.Second,
	}
	c, err := client.New(cfg)
	if err != nil {
		log.Fatal(err)
	}
	kapi := client.NewKeysAPI(c)
	// get "/foo" key's value
	key := fmt.Sprintf("/%s/%s", *config.EtcdRootPath, *config.EtcdServiceName)
	log.Info("Getting ", key)
	resp, err := kapi.Get(context.Background(), key, nil)
	// resp, err = kapi.Get(context.Background(), "/foo", nil)
	if err != nil {
		log.Error(err)
	} else {
		// print common key info
		log.Infof("Get is done. Metadata is %q\n", resp)
		// print value
		log.Infof("%q key has %q value\n", resp.Node.Key, resp.Node.Value)
	}
}

//GetIPAddr 获取本机IP
func GetIPAddr() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				// os.Stdout.WriteString(ipnet.IP.String() + "\n")
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

//PrintSysInfo 打印系统信息
func PrintSysInfo() {
	log := GetLogger()
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	log.Notice("----------------------------------------------------------")
	log.Notice("Cur go routines: ", runtime.NumGoroutine())
	log.Notice("Cur memuseage: ")
	log.Noticef("\tAlloc = %v MiB", bToMb(m.Alloc))
	log.Noticef("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	log.Noticef("\tSys = %v MiB", bToMb(m.Sys))
	log.Noticef("\tNumGC = %v\n", m.NumGC)
	log.Notice("----------------------------------------------------------")
}

var fCPU, fMem *os.File
//ProfileStart 开始打印系统调用信息
func ProfileStart() {
	log := GetLogger()
	log.Notice("Start recording profile!!")
	config := GetConfig()
	var err error
	fCPU, err = os.Create(*config.CPUProfilePtr)
	CheckError(err, "open CPU profile")

	fMem, err = os.Create(*config.MEMProfilePtr)
	CheckError(err, "open memory profile")

	pprof.StartCPUProfile(fCPU)
	pprof.WriteHeapProfile(fMem)
}

//ProfileStop 系统调用结束
func ProfileStop() {
	log := GetLogger()
	log.Notice("End recording profile!!")
	pprof.StopCPUProfile()
	fCPU.Close()
	fMem.Close()
}
