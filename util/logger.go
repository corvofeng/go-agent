package util

import (
	"github.com/op/go-logging"
	"os"
)

var (
	log    = logging.MustGetLogger("example")
	format = logging.MustStringFormatter(
		// `%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
		`%{color}%{time:15:04:05} %{shortfunc:9s} ▶ %{level:.4s} %{color:reset} %{message}`,
	)
)

// 初始化Logger, 未来可以使用文件进行初始化.
func InitLogger() {
	backend := logging.NewLogBackend(os.Stdout, "", 0)
	backendFormatter := logging.NewBackendFormatter(backend, format)
	logging.SetBackend(backendFormatter)

	config := GetConfig()

	switch *config.Loglvl {
	case "INFO":
	logging.SetLevel(logging.INFO, "example")
	break

	case "DEBUG":
	logging.SetLevel(logging.DEBUG, "example")
	break
	default:
		log.Notice("Unkonw level flags")
	}
}

// 全局使用的Logger
func GetLogger() *logging.Logger {
	return log
}
