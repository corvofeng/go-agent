package dubbogo

import (
	"fmt"
	// "io"
	// "net/http"
	"bufio"
	"net"
	"sync"
	// "bytes"
	"encoding/binary"
	"encoding/hex"
	"sync/atomic"
)

import util "../util"

type FutureResponse struct {
	Id       uint64
	HasWrite bool
	Response chan string
	CliId    uint64 // 为了清理时能够找到对应的客户端
}

type ICallback interface {
	onResponse(string)
}

var ids uint64

/**
 * 从id 对应 channel变量, 在请求后, 等待channel变量返回
 *   id => channel
 */
var mID2Future sync.Map //[uint64](*FutureResponse)

var (
	id2Future map[uint64]*FutureResponse
	idLock    sync.Mutex
)

//RpcClient var rpcInfo chan
type RpcClient struct {
	curId     uint64
	conn      *net.TCPConn
	id2Future sync.Map
}

func createMap(size uint64) {
	id2Future = make(map[uint64]*FutureResponse, size)
}

func GetFutureResponseFromPool() {

}

const cliCnt uint64 = 6

var clients [cliCnt]RpcClient

//RequestOver 请求结束后释放该对象, 并关闭channel
func RequestOver(fResponse *FutureResponse) {
	id = fResponse.Id
	// delete(mID2Future, id)
	client := &clients[fResponse.CliId]
	client.id2Future.Delete(id)
	close(fResponse.Response)
}

//MakeFutureResponse 新建一个futureResponse对象, 并将其添加到map中
func MakeFutureResponse() (fResponse *FutureResponse, err error) {
	var log = util.GetLogger()
	cliId := ids % cliCnt // 首先获取将要使用的客户端
	client := &clients[cliId]

	fResponse = &FutureResponse{
		Id:       client.curId,
		Response: make(chan string),
		HasWrite: false,
		CliId:    cliId,
	}

	client.id2Future.Store(client.curId, fResponse) // 存储至对应的client中
	atomic.AddUint64(&ids, 1)          // 很可能被多个routine调用, 必须安全
	atomic.AddUint64(&client.curId, 1) // 很可能被多个routine调用, 必须安全

	if ids == 1 {
		config := util.GetConfig()
		CreateAsyncDubboConnect(fmt.Sprintf("127.0.0.1:%d", *config.DubboPortPtr))
		LoopRun()
	}

	if ids % 3000 == 0 { // 每创建1000个对象, 开始打印当前状态信息
		util.PrintSysInfo()
	}
	log.Debugf("Create %d:%d ok!!", cliId, fResponse.Id)
	return fResponse, nil
}

//CreateAsyncDubboConnect 开启所有的连接
func CreateAsyncDubboConnect(tcpAddress string) {
	var err error
	// var log = util.GetLogger()
	for i := uint64(0); i < cliCnt; i++ {
		clients[i].conn, err = SenderConnect(tcpAddress)
		util.CheckError(err, "connect")
	}
}

//MakeAsyncReq 向对应的客户端中发送异步请求
func MakeAsyncReq(fResponse *FutureResponse, service *DubboCtx) int {

	var log = util.GetLogger()
	var stream = service
	var client = &clients[fResponse.CliId] // 获取对应的客户端

	path := stream.Service
	method := stream.Method
	args := service.Args
	decodeClsMap := service.JavaClassMap
	var encoderClsMap map[string]string = make(map[string]string)
	for k, v := range decodeClsMap {
		encoderClsMap[v.Name()] = k
	}

	bodyBytes, _ := BufferBody(service.DubboVer, path, method, service.Version, args, service.Timeout, encoderClsMap)
	headBytes, _ := bufferHeaderID(uint32(len(bodyBytes)), fResponse.Id)

	headBytes = append(headBytes, bodyBytes...)

	log.Debugf("Require to coon %d", fResponse.CliId)

	writeLength, err := client.conn.Write(headBytes)
	if util.CheckError(err, "chatSend") == false {
		return writeLength
	}
	return 0
}

//最重要的一点就是需要处理粘包
func readerMain(client *RpcClient, cliID uint64) {
	var log = util.GetLogger()
	reader := bufio.NewReader(client.conn)
	headerLength := 16
	for {
		lengthByte, err := reader.Peek(headerLength)
		// lengthBuff := bytes.NewBuffer(lengthByte[12:])
		// log.Debug(hex.EncodeToString(lengthByte[12:]))

		id := binary.BigEndian.Uint64(lengthByte[4:12])
		length := binary.BigEndian.Uint32(lengthByte[12:])

		log.Debugf("Get id: %d", id)
		log.Debugf("Get length: %d", length)

		pack := make([]byte, headerLength+int(length))
		_, err = reader.Read(pack)
		util.CheckError(err, "can't read")

		log.Debug("Get data: ", hex.EncodeToString(pack))

		fResponse, ok := client.id2Future.Load(id)
		if ok && !fResponse.(*FutureResponse).HasWrite {
			fResponse.(*FutureResponse).Response <- string(pack[headerLength+2 : headerLength+int(length)-1])
			fResponse.(*FutureResponse).HasWrite = true
		} else {
			log.Errorf("can't find  %d : %d ", cliID, id)
		}
	}
}

//LoopRun 从返回的消息中读取
// 每个连接池的处理返回数据
func LoopRun() {
	for i := uint64(0); i < cliCnt; i++ {
		go readerMain(&clients[i], i)
	}
}

func GraceClose() {
	for i := uint64(0); i < cliCnt; i++ {
		clients[i].conn.Close()
	}
}
