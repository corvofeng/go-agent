package dubbogo

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"reflect"
	"strconv"
	"time"
)

import util "../util"

var id uint64

const (
	gDefaultLen      = 8388608 // 8 * 1024 * 1024 default body max length
	gPacketDefultLen = 131400  // 128 * 1024 default one buf max length

	gStrSplit byte = 0x0a // 请求报文中需要添加间隔符

	Response_OK                byte = 20
	Response_CLIENT_TIMEOUT    byte = 30
	Response_SERVER_TIMEOUT    byte = 31
	Response_BAD_REQUEST       byte = 40
	Response_BAD_RESPONSE      byte = 50
	Response_SERVICE_NOT_FOUND byte = 60
	Response_SERVICE_ERROR     byte = 70
	Response_SERVER_ERROR      byte = 80
	Response_CLIENT_ERROR      byte = 90

	RESPONSE_WITH_EXCEPTION int32 = 0
	RESPONSE_VALUE          int32 = 1
	RESPONSE_NULL_VALUE     int32 = 2
)

type (
	DubboCtx struct {
		DubboVer     string
		Service      string
		Method       string
		Version      string
		Args         map[string]interface{}
		Timeout      int
		JavaClassMap map[string]reflect.Type
		Return       reflect.Type
	}
)

func SenderConnect(tcpaddr string) (*net.TCPConn, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp4", tcpaddr)
	if util.CheckError(err, "ResolveTCPAddr") == false {
		return nil, err
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if util.CheckError(err, "DialTCP") == false {
		return nil, err
	}

	return conn, nil
}

func SendHession(tcpaddr string, service *DubboCtx) (interface{}, error) {
	var stream = service
	//封装hessian over tcp 请求

	path := stream.Service
	method := stream.Method
	args := service.Args
	decodeClsMap := service.JavaClassMap
	// retType := service.Return

	//classMap 反转
	//goClassName---JavaClassName 用于encode
	//JavaClassName---goClassName 用于decode
	var encoderClsMap map[string]string = make(map[string]string)
	for k, v := range decodeClsMap {
		encoderClsMap[v.Name()] = k
	}
	bodyBytes, _ := BufferBody(service.DubboVer, path, method, service.Version, args, service.Timeout, encoderClsMap)
	headBytes, _ := bufferHeader(len(bodyBytes))

	headBytes = append(headBytes, bodyBytes...)

	tcpAddr, err := net.ResolveTCPAddr("tcp4", tcpaddr)
	if util.CheckError(err, "ResolveTCPAddr") == false {
		return nil, err
	}
	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if util.CheckError(err, "DialTCP") == false {
		return nil, err
	}
	defer conn.Close()
	//conn write
	writeLength, err := conn.Write(headBytes)
	if util.CheckError(err, "chatSend") == false {
		return nil, err
	}
	fmt.Println(conn.RemoteAddr().String(), "send lens: ", writeLength)
	//conn read
	buf, retLengh, err := handleConnection(conn, service.Timeout)
	//retLengh, err := conn.Read(buf)
	if util.CheckError(err, "chatRead") == false {
		return nil, err
	}

	fmt.Println(conn.RemoteAddr().String(), "received lens: ", retLengh)
	// ret, err := decodeRespon(buf[0:retLengh], decodeClsMap)
	retStr, err := decodeStr(buf[0:retLengh], decodeClsMap)

	if util.CheckError(err, "recv") == false {
		return nil, err
	}

	// fmt.Println("ret str " + retStr)
	// retObject := retReflect(ret, retType)
	return retStr, err
}

func HandleAsyncConnection(conn net.Conn, timeout int) ([]byte, int, error) {
	ret := make([]byte, gDefaultLen)
	buffer := make([]byte, gPacketDefultLen)
	offset := 0
	var err error = nil

	var bodyLength int
	var errCh chan int = make(chan int, 1)
	var ch chan int = make(chan int, 1)

	go func() {
		for {
			n, err := conn.Read(buffer)
			if err != nil {
				errCh <- 1
				//return nil, 0, err
			}
			copy(ret[offset:], buffer[:n])
			offset = offset + n

			if offset >= 16 {
				bodyLength = int(ret[12])<<24 + int(ret[13])<<16 + int(ret[14])<<8 + int(ret[15])
			}
			if offset >= bodyLength+16 {
				ch <- 1
				//return ret, offset, nil
			}
		}
	}()

	select {
	case <-ch: //返回结果
	case <-errCh:
	case <-time.After(time.Millisecond * time.Duration(timeout)): //超时5s
		err = errors.New("timeout after " + strconv.Itoa(timeout) + " ms")
	}
	return ret, offset, err

}

func handleConnection(conn net.Conn, timeout int) ([]byte, int, error) {
	ret := make([]byte, gDefaultLen)
	buffer := make([]byte, gPacketDefultLen)
	offset := 0
	var err error = nil

	var bodyLength int
	var errCh chan int = make(chan int, 1)
	var ch chan int = make(chan int, 1)

	go func() {
		for {
			n, err := conn.Read(buffer)
			if err != nil {
				errCh <- 1
				//return nil, 0, err
			}
			copy(ret[offset:], buffer[:n])
			offset = offset + n

			if offset >= 16 {
				bodyLength = int(ret[12])<<24 + int(ret[13])<<16 + int(ret[14])<<8 + int(ret[15])
			}
			if offset >= bodyLength+16 {
				ch <- 1
				//return ret, offset, nil
			}
		}
	}()
	select {
	case <-ch: //返回结果
	case <-errCh:
	case <-time.After(time.Millisecond * time.Duration(timeout)): //超时5s
		err = errors.New("timeout after " + strconv.Itoa(timeout) + " ms")
	}
	return ret, offset, err
}

func decodeStr(buff []byte, clsMap map[string]reflect.Type) (string, error) {
	length := len(buff)

	if buff[3] != Response_OK {
		fmt.Println("Response not OK", string(buff[18:length]))
		return "", errors.New("Response not OK")
	}
	return string(buff[18 : length-1]), nil

}

//creat request buffer body
func BufferBody(dver string, path string, method string, version string, args map[string]interface{}, timeout int, clsMap map[string]string) (body []byte, err error) {

	//模拟dubbo的请求
	buff := bytes.NewBuffer(nil)
	jDver, _ := json.Marshal(dver)
	jPath, _ := json.Marshal(path)
	jMethod, _ := json.Marshal(method)
	jParamTypes, _ := json.Marshal(args["parameterTypesString"])
	jParam, _ := json.Marshal(args["parameter"])

	pathMap := make(map[string]string)
	pathMap["path"] = path

	jPather, _ := json.Marshal(pathMap)

	buff.Write(jDver)

	buff.WriteByte(gStrSplit)
	buff.Write(jPath)
	buff.WriteByte(gStrSplit)
	buff.WriteString(version)
	buff.WriteByte(gStrSplit)
	buff.Write(jMethod)
	buff.WriteByte(gStrSplit)
	buff.Write(jParamTypes)
	buff.WriteByte(gStrSplit)
	buff.Write(jParam)
	buff.WriteByte(gStrSplit)

	buff.Write(jPather)
	buff.WriteByte(gStrSplit)

	implicitArgs := make(map[string]string)
	implicitArgs["path"] = path
	implicitArgs["interface"] = path
	implicitArgs["version"] = version
	implicitArgs["timeout"] = strconv.Itoa(timeout)

	// encoder.WriteObject(implicitArgs)

	return buff.Bytes(), nil
}

/*
func getTypes(args map[string]interface{}) string {

	var types string = ""
	// 初始化 + 赋值, 基本类型的序列化tag
	typeRef := map[string]string{
		"boolean": "Z",
		"int":     "I",
		"short":   "S",
		"long":    "J",
		"double":  "D",
		"float":   "F",
	}

	// 遍历map
	for k, _ := range args {
		if !strings.Contains(k, ".") {
			types += typeRef[k]
		} else {
			types = types + "L" + strings.Replace(k, ".", "/", -1) + ";"
		}
	}

	return types
}
*/

//creat request buffer head
func bufferHeader(len int) ([]byte, error) {
	// magic code
	var head = []byte{0xda, 0xbb, 0xc6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	var i = 15
	if len > gDefaultLen {
		fmt.Printf(`Data length too large: ${length}, max payload: ${DEFAULT_LEN}`)
	}

	for len >= 256 {
		head[i] = byte(len % 256)
		len >>= 8
		i--
	}
	head[i] = byte(len % 256)
	return head, nil

}

func bufferHeaderID(len uint32, id uint64) ([]byte, error) {
	// magic code
	var head = []byte{0xda, 0xbb, 0xc6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	binary.BigEndian.PutUint64(head[4:12], id)
	binary.BigEndian.PutUint32(head[12:16], len)
	return head, nil
}
